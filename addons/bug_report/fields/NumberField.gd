tool
extends "./InputField.gd"

var old_text = ""
var isNumber = RegEx.new()
var numbers = ["0","1","2","3","4","5","6","7","8","9"]
var allow_dot = true

func _init(fieldName: String, labelStr: String, initialValue: String = "", required: bool = false).(fieldName, labelStr, initialValue, required):
	old_text = initialValue

func _reset():
	field.text = old_text

func _on_text_changed(newText: String):
	var foundDot = false
	for character in newText:
		if character == '.':
			if not allow_dot:
				return _reset() 
			if not foundDot:
				foundDot = true
			else:
				return _reset()
		if not (character in numbers):
			return _reset()
	old_text = newText
	._on_text_changed(newText)

func get_value():
	return float(get_text_value())
