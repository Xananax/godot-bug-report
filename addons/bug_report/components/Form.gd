extends VBoxContainer

var Fields = preload("../fields/Fields.gd").new()
var InputField = preload("../fields/InputField.gd")
var fieldsContainer: VBoxContainer
var button: Button = Button.new()
var is_valid: bool = true
var serialized: Dictionary = {}
var disabled setget set_disabled, get_disabled
signal submit(serialized)

func _init():
	fieldsContainer = VBoxContainer.new()
	add_child(fieldsContainer)
	button.text = "submit"
	var _err = button.connect("pressed", self, "_on_submit")
	add_child(button)

func set_fields(fields: Array):
	for field in fields:
		var fieldNode = Fields.create_field(field) if not (field is InputField) else field
		fieldsContainer.add_child(fieldNode)

func validate():
	var formValid = true
	for field in fieldsContainer.get_children():
		var _fieldValid = field.update_validated_status()
		if formValid and not _fieldValid:
			formValid = false
	is_valid = formValid
	return is_valid

func serialize() -> void:
	var obj = {}
	for field in fieldsContainer.get_children():
		var serialized_field = field.get_serialized_value()
		var value = serialized_field.value
		var name = serialized_field.name
		obj[name] = value
	serialized = obj

func set_disabled(_disabled: bool) -> void:
	button.disabled = _disabled

func get_disabled() -> bool:
	return button.disabled

func _on_submit():
	if not validate():
		# TODO: show some message
		return
	serialize()
	emit_signal("submit",serialized)