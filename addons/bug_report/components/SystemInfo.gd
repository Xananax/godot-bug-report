extends Node

var is_joypad = false
var joypad_index = -1

func _init():
	var _err = Input.connect("joy_connection_changed", self, "_on_joy_connection_changed")

func get_info():
	var info = {
		"OS": OS.get_name(),
		"datetime": OS.get_datetime(),
		"video_driver": OS.get_video_driver_name(OS.get_current_video_driver()),
		"video_adapter": VisualServer.get_video_adapter_name(),
		"video_vendor": VisualServer.get_video_adapter_vendor(),
		"screen_size": OS.get_screen_size(),
		"screen_dpi": OS.get_screen_dpi(),
		"cores": OS.get_processor_count(),
		"locale": OS.get_locale(),
		"joypad": Input.get_joy_name(joypad_index) if is_joypad else "" 
	}
	return info

func _on_joy_connection_changed(device_id, connected):
	is_joypad = connected
	joypad_index = device_id

