tool
extends Control

var formFields = [] 

var server_url = "http://127.0.0.1:3000"
var use_https = false

var http = HTTPRequest.new()
var form = preload("./components/Form.gd").new()
var systemInfo = preload('./components/SystemInfo.gd').new()

func _init(_server_url, _formFields, _use_https):
	server_url = _server_url
	use_https = _use_https
	form.set_fields(_formFields)

func _ready():
	add_child(http)
	add_child(form)
	var _err1 = form.connect("submit", self, "_on_submit")
	var _err2 = http.connect("request_completed", self, "_on_http_request_completed")

func _make_post_request(url: String, data_to_send: Dictionary, use_ssl: bool):
	
	var query = JSON.print(data_to_send)
	var headers = [ "Content-Type: application/json" ]
	var status = http.request(url, headers, use_ssl, HTTPClient.METHOD_POST, query)
	if status != OK:
		push_error("An error occurred in the local HTTP request.")
	else:
		print("request sent")

func _on_http_request_completed(result: int, response_code: int, headers: PoolStringArray, body: PoolByteArray):
	form.disabled = false
	match(result):
		HTTPRequest.RESULT_SUCCESS:
			var response = body.get_string_from_utf8()
			print("response: ", response, ", response code: ", response_code, ", headers: ", headers)
		HTTPRequest.RESULT_CHUNKED_BODY_SIZE_MISMATCH:
			push_error("RESULT_CHUNKED_BODY_SIZE_MISMATCH")
		HTTPRequest.RESULT_CANT_CONNECT:
			push_error("RESULT_CANT_CONNECT")
		HTTPRequest.RESULT_CANT_RESOLVE:
			push_error("RESULT_CANT_RESOLVE")
		HTTPRequest.RESULT_CONNECTION_ERROR:
			push_error("RESULT_CONNECTION_ERROR")
		HTTPRequest.RESULT_SSL_HANDSHAKE_ERROR:
			push_error("RESULT_SSL_HANDSHAKE_ERROR")
		HTTPRequest.RESULT_NO_RESPONSE:
			push_error("RESULT_NO_RESPONSE")
		HTTPRequest.RESULT_BODY_SIZE_LIMIT_EXCEEDED:
			push_error("RESULT_BODY_SIZE_LIMIT_EXCEEDED")
		HTTPRequest.RESULT_REQUEST_FAILED:
			push_error("RESULT_REQUEST_FAILED")
		HTTPRequest.RESULT_DOWNLOAD_FILE_CANT_OPEN:
			push_error("RESULT_DOWNLOAD_FILE_CANT_OPEN")
		HTTPRequest.RESULT_DOWNLOAD_FILE_WRITE_ERROR:
			push_error("RESULT_DOWNLOAD_FILE_WRITE_ERROR")
		HTTPRequest.RESULT_REDIRECT_LIMIT_REACHED:
			push_error("RESULT_REDIRECT_LIMIT_REACHED")
		HTTPRequest.RESULT_TIMEOUT:
			push_error("RESULT_TIMEOUT")

func _on_submit(object: Dictionary):
	form.disabled = true
	object.system = systemInfo.get_info()
	_make_post_request(server_url, object, use_https)
