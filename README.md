# BugReport <!-- omit in toc -->

## What Does This Do? <!-- omit in toc -->

This addon:

- Allows easy creation of forms
- Has a simple system that sends the content of the form to a URL of your choice
- Appends to it some stats from the user's system

**Table of Contents**

- [How to Try This](#how-to-try-this)
- [Code Overview](#code-overview)
- [Development](#development)
	- [How to add a Field:](#how-to-add-a-field)
	- [How to edit the Form:](#how-to-edit-the-form)
	- [How Edit the Sent Request or UI Feedback For the User](#how-edit-the-sent-request-or-ui-feedback-for-the-user)
	- [How to append data to what the form sends](#how-to-append-data-to-what-the-form-sends)

## How to Try This

There's a small test python 3 server, to use it:

```bash
python server.py
```
This server doesn't do anything; it will just log any GET or POST request sent to it, to verify that Godot is sending the data properly. So, keep an eye on the console!

Then, run the Godot project; try entering wrong values (i.e, badly formed email), and submitting; it won't work. Then enter proper values, and submit the form again; you should see a log in the Python console with the values you entered, and the system info.

## Code Overview

If you open `Main.gd`, you will see, at the top, the following values:

```js
export var formFields = [
	{ "type": "email",
		"name": "email"
	},
	{ "name": "name", 
		"value": "Andrew",
		"required": true
	}
] 

export var server_url = "http://127.0.0.1:3000"
```

To create fields, the only mandatory field prop is `"name"`. Optional fields are `"type"` (currently, only `"email"`, others may follow), and `"required"`. The file demonstrates different types of fields, including creating a custom one.

`server_url` is set to the mock Python server, change it to your real API

## Development

Although this is in "res://addons" for convenience, it is not an editor plugin, and won't show in the list of addons.

This is also not an addon that you're supposed to just slot in your project and edit through data; providing proxying functions to all the different ways a form could be made would just be exponentially complex; rather, this is a _toolbox_, that you're supposed to extend.

### How to add a Field:

1. Create a new field under `res://addon/bug_report/fields`
2. Make it extend `InputField.gd`
3. Add it to the list of fields in `res://addon/bug_report/fields/Field.gd`
4. Check out the examples such as `EmailField` or `NumberField` to see how it's done. You can also read the source of `InputField`

### How to edit the Form:

Edit `res://addon/bug_report/components/Form.gd` to change how the form looks. That's also where you'd add some logic to show a message when the form is invalid, if needed

### How Edit the Sent Request or UI Feedback For the User 

Edit `BugReport.gd` under `res://addon/bug_report`. You can edit `_make_post_request` to change the form's sending behavior, and `_on_http_request_completed` to show messages and such when it finishes.

### How to append data to what the form sends

System stats are already appended; To edit those, check `res://addon/bug_report/components/SystemInfo.gd`. You can add additional logic there, or hook into `res://addon/bug_report/BugReport.gd`'s `_make_post_request` method