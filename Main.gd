extends Node

var formFields = [
	{ "type": "email",
		"name": "email"
	},
	{ "name": "name", 
		"value": "Andrew",
		"required": true
	},
	{ "name": "age", 
		"value": 12,
		"type":"number",
		"required": true
	},
	CustomInputField.new("test", "test")
]

var server_url = "http://127.0.0.1:3000"
var use_https = false

var BugReport = preload("res://addons/bug_report/BugReport.gd").new(server_url, formFields, use_https)

func _ready():
	add_child(BugReport)

class CustomInputField extends "res://addons/bug_report/fields/InputField.gd":

	func _init(fieldName: String, labelStr: String, initialValue: String = "", required: bool = false).(fieldName, labelStr, initialValue, required):
		add_message("this field keeps expanding everytime you write in it")
		pass

	func _on_text_changed(new_text: String):
		add_message(new_text)
