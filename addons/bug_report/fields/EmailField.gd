tool
extends "./InputField.gd"

func _init(fieldName: String, labelStr: String, initialValue: String = "", required: bool = false).(fieldName, labelStr, initialValue, required):
	pass
	
func validate(first_run: bool) -> bool:
	if first_run:
		return true
	var text = get_text_value();
	var atIndex = text.find("@")
	var dotIndex = text.find_last(".")
	var maxLength = text.length() -1
	return atIndex > 0 and \
		atIndex < maxLength and \
		dotIndex > atIndex + 1 and \
		dotIndex < maxLength

func _became_valid():
	remove_message("please enter a valid email")
	
func _became_invalid():
	add_message("please enter a valid email")
