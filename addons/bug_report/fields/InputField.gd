tool
# This class is a simple text field and is expected to be extended by other fields
extends VBoxContainer

var debounce_timer: Timer = Timer.new()
var fieldContainer:HBoxContainer = HBoxContainer.new()
var messages:VBoxContainer = VBoxContainer.new()
var fieldLabel:Label = Label.new()
var requiredLabel:Label = Label.new()
var field:LineEdit = LineEdit.new()
var is_valid: bool = true
var is_required_fullfilled: bool = true
var is_required: bool = false

# time before a field gets validated
export var debounce_delay_Second: float = .3 setget set_debounce_time, get_debounce_time

func _init(fieldName: String, labelStr: String, initialValue: String = "", required: bool = false):
	name = fieldName
	fieldLabel.text = labelStr
	requiredLabel.text = "*" if required == true else ""
	field.text = initialValue
	is_required = required
	var _err1 = field.connect("text_changed", self, "_on_text_changed")
	var _err2 = debounce_timer.connect("timeout", self, "update_validated_status")
	debounce_timer.one_shot = true
	debounce_timer.wait_time = debounce_delay_Second
	debounce_timer.stop()
	add_child(debounce_timer)
	add_child(fieldContainer)
	fieldContainer.add_child(fieldLabel)
	fieldContainer.add_child(requiredLabel)
	fieldContainer.add_child(field)
	add_child(messages)
	var _unused = update_validated_status(true)

# time before a field gets validated
func set_debounce_time(time: float) -> void:
	debounce_timer.wait_time = time

# time before a field gets validated
func get_debounce_time() -> float:
	return debounce_timer.wait_time

# adds a message to a list of messages below the input
# messages are just appended in FIFO mode
func add_message(message: String) -> void:
	for child in messages.get_children():
		if child.text == message:
			return
	var newMessage = Label.new()
	newMessage.text = message
	messages.add_child(newMessage)

# this is used by the form to obtain the field's name
# and value
func get_serialized_value():
	return { "name": name, "value": get_value()}

# messages are removed by value, to keep it simple
# add_message("hello world") is removed with remove_message("hello world")
func remove_message(message: String) -> void:
	for child in messages.get_children():
		if child.text == message:
			messages.remove_child(child)
			return

# Returns the untransformed value
func get_text_value() -> String:
	return field.get_text()

# this method is an occasion to transform user input
# before serialization. For example, if you want a number field,
# here would be the place to cast the string to a number
func get_value():
	return get_text_value()

# Any validation rule should be inserted in here
func validate(_first_run: bool) -> bool:
	return true

# this runs a little while after the user finishes typing
# it verifies the field is valid
func _validate_field_contents(first_run: bool) -> bool:
	var _is_valid = validate(first_run)
	if _is_valid != is_valid:
		is_valid = _is_valid
		if is_valid:
			_became_valid()
		else:
			_became_invalid()
	return is_valid

# this runs a little while after the user finishes typing
# it verifies the field is not required, _or_ that it is required and has a value
func _validate_required_status(first_run: bool) -> bool:
	var _is_required_fullfilled = false if is_required and get_text_value().length() == 0 else true
	if _is_required_fullfilled != is_required_fullfilled:
		is_required_fullfilled = _is_required_fullfilled
		if first_run:
			return true
		if is_required_fullfilled:
			_became_invalid_required()
		else:
			_became_valid_required()
	return is_required_fullfilled

# this runs a little while after the user finishes typing
# it also runs from the form on submit
# it verifies the field is validated and, if it's required that it is not empty
func update_validated_status(first_run: bool = false) -> bool:
	var required = _validate_required_status(first_run)
	var validated = _validate_field_contents(first_run)
	return required and validated

# this runs if the field was required and empty, but isn't anymore
func _became_valid_required():
	remove_message("this field is required")

# this runs if the field was required and filled, but is now empty
func _became_invalid_required():
	add_message("this field is required")

# this runs if the field was invalid previously, but is now valid
func _became_valid():
	add_message("this field is invalid")

# this runs if the field was valid previously, but is now invalid
func _became_invalid():
	remove_message("this field is invalid")

# this runs each time the field is edited.
func _on_text_changed(_new_text: String):
	debounce_timer.start()