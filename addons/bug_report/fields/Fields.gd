extends Node

var EmailField = preload("./EmailField.gd")
var InputField = preload("./InputField.gd")
var NumberField = preload("./NumberField.gd")

func create_field(field):
	var type = field.type if "type" in field else ""
	var required = field.required if "required" in field else false
	var name = field.name
	var label = field.label if "label" in field else name
	var value = str(field.value) if "value" in field else ""
	match(type):
		"email": return EmailField.new(name, label, value, required)
		"number":return NumberField.new(name, label, value, required)
		_:       return InputField.new(name, label, value, required)


